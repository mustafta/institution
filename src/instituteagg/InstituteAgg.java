/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package instituteagg;
import java.util.ArrayList;
/**
 *
 * @author Taghreed
 */
public class InstituteAgg {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Student student1 = new Student("Alex", "SE", 123);
        Student student2 = new Student("Sara", "SDNE", 423);
        Student student3 = new Student("John", "SE", 293);
         Student student4 = new Student("Smith", "SDNE", 193);
        
        //student list for SE department 
        ArrayList<Student> SE_Student = new ArrayList<Student>();
        SE_Student.add(student1);
        SE_Student.add(student3);
        
         //student list for SDNE department 
        ArrayList<Student> SDNE_Student = new ArrayList<Student>();
        SDNE_Student.add(student2);
        SDNE_Student.add(student4);
        
        Department SE = new Department("SE", SE_Student);
        Department SDNE = new Department("SDNE", SDNE_Student);
        
        ArrayList<Department> departments = new ArrayList<Department>();
        departments.add(SE);
        departments.add(SDNE);
        
        
        Institute inst = new Institute("CS", departments);
         System.out.println("Total Students in the Institue : "+ inst.getTotalStudent ());
        
        }
    
}
