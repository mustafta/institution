/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package instituteagg;
import java.util.ArrayList;
/**
 *
 * @author Taghreed
 */
public class Institute {
    String instituteName;
    private ArrayList<Department> departments;
    
    public Institute (String instituteName, ArrayList<Department> departments)
    {
    this.instituteName=instituteName;
    this.departments=departments;
    
    }
    
    
    public String getInstituteName()
    {
    
    return instituteName;
    } 
    public ArrayList<Department> getDepartments()
    {
    
    return departments;
    }
    
    public int getTotalStudent ()
    {
        int numberOfStudent = 0;
        ArrayList<Student> students;
        for (Department dept: departments)
        {
        students = dept.getStudents();
        
        for(Student s: students)
        {
            numberOfStudent++;
        
        }
        }
        return numberOfStudent;
    }
    }
    

