/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package instituteagg;
import java.util.ArrayList;
/**
 *
 * @author Taghreed
 */
public class Department {
    private String depName;
    private ArrayList <Student> students;
       
    
    //public Department(){}
    
    public Department (String depName, ArrayList <Student> students)
    {
    this.depName=depName;
    this.students=students;
    
    }
    
    public String getDepName()
    {
    
    return depName;
    } 
    public ArrayList<Student> getStudents()
    {
    
    return students;
    }
    
    
}
