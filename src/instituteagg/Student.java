/*
 * Taghreed Safaryan
 *  991494905
 * 
 */
package instituteagg;


/**
 *
 * @author Taghreed
 */
public class Student {
    private String studentName;
    private String department;
    private int studentId;
    
    
    //public Student(){}
    
    public Student (String studentName, String department, int studentId)
    {
    this.studentName=studentName;
    this.department=department;
    this.studentId=studentId;
    }
    
    public String getStudentName()
    {
    
    return this.studentName;
    }
    
      
    public String getDepartment()
    {
    
    return this.department;
    }
    public int getStudentId()
    {
    
    return this.studentId;
    } 
    
}
